# CSGO data recording plugin
A sourcemod plugin written in the SourcePawn language to record various data during CSGO gameplay into CSV or stream it on an LSL outlet.

# Requirements

## metamod
https://www.sourcemm.net/


## source mod (requires metamod)
https://www.sourcemod.net/

## steamworks
https://users.alliedmods.net/~kyles/builds/SteamWorks/

## CSGO LSL extension
https://gitlab.unige.ch/sims/lsl-modules/csgo-lsl-extension

# load/unload plugin
`m plugins load/unload <plugin_name>`


# plugin compilation and installation

To compile the plugin, copy `datarecorder.sp` to `srcds/csgo/addons/sourcemod/scripting`, navigate to that directory and run `compile.exe` which will compile all plugins and generat a `.smx` file.


To install the plugin copy the `compiled/datarecorder.smx` file to `srcds/csgo/addons/sourcemod/plugins`.

# Notes on code

onplayerdeath():
for rewards like give health or armor

update(): where the data on the tick is updated

# Plugin server commands
* datarec_lsl: enables/disables LSL
* datarec_pipe: enables/disables writing data to pipe
* datarec_csv: enables/disables writing data to csv
* datarec_auto: enables/disables auto starting the data capturing once the round starts
* datarec_start: starts data capturing
* datarec_server: sends a server command as argument

# Notes on data

## LSL
The following outlets are created for each player:
* IntData\_\<playerID\>: A constant rate integer data outlet (rate depends on server fps setting)
* FloatData\_\<playerID\>: A constant rate float data outlet (rate depends on server fps setting)
* MarkerData\_\<playerID\>: A marker outlet

### integer data
|channel| Label              | Format | Notes                                   |
|-------|--------------------|--------|-----------------------------------------|
|0      |health              |Decimal |Health of player |
|1      |armor               |Decimal |Armor of player |
|2      |score               |Decimal |Score of player |

### float data
|channel| Label              | Format | Notes                                   |
|-------|--------------------|--------|-----------------------------------------|
|0      |time                |Float   |Timestamp since start of logs in seconds |
|1      |positionX           |Float   |Player eye position x |
|2      |positionY           |Float   |Player eye position y|
|3      |positionZ           |Float   |Player eye position z |

### markers
Some marker information is sent at every round start in a JSON-like format:
* Players

## CSV (and pipe)

### GameData\_\<playerID\>\_\<time\>(.csv)
|column | Label              | Format | Notes                                   |
|-------|--------------------|--------|-----------------------------------------|
|0-2   |LSL integer data    |Decimal |See LSL integer data |
|3-6   |LSL float data      |Float   |See LSL float data|

### Players_\<time\>.csv
|column | Label              | Format | Notes                                   |
|-------|--------------------|--------|-----------------------------------------|
|0      |id                  |Decimal |player ID |
|1      |name                |String  |player name|
|2      |team                |Decimal |player's team index|
|3      |isBot               |Boolean |indicates if this player is a bot|