#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <SteamWorks>
#include <string>
#include <LSLext> // Must include the LSL extension

#pragma newdecls required
#pragma dynamic 131072

/**
 * Plugin public information.
 */
public Plugin myinfo =
{
	name = "Data Recorder Plugin",
	author = "mfanourakis",
	description = "CS:GO LSL plugin example",
	version = "1.0",
	url = ""
};

// An enum for easy indexing of samples with integer values
enum LSLintChnIndex {
	Health=0, Armor, Score
};
// Array with the channel names for the integer data outlet, order should match the above enum
char lslIntChnLabels[][16] = {
	"health", "armor", "score"
};

// An enum for easy indexing of samples with float values
enum LSLfltChnIndex {
	Time = 0, PositionX, PositionY, PositionZ
};
// Array with the channel names for the integer data outlet, order should match the above enum
char lslFltChnLabels[][16] = {
	"time", "positionX", "positionY", "positionZ"
};


bool pipeEnableVal = false; // Only for UNIX systems
bool csvEnableVal = false;
bool lslEnableVal = false;
bool autoRecordVal = false;

// --- data recording bools ---
bool recording = false;
bool started = false;

//Data handles and IDs
char csvpath[4][PLATFORM_MAX_PATH];
char pipepath[4][PLATFORM_MAX_PATH]; // Only for UNIX systems
File csvHandles[4] = {null, null, null, null};
File pipeHandles[4] = {null, null, null, null}; // Only for UNIX systems
char lslStreamIDs[4][3][64]; //3 outlets per player: one for int data, one for float data, one for string/marker data

//Player info
int players[4] = {-1,-1,-1,-1};
char playerName[sizeof(players)][128];

//Data
int lslIntSample[sizeof(players)][view_as<int>(LSLintChnIndex)+1];
float lslFltSample[sizeof(players)][view_as<int>(LSLfltChnIndex)+1];

char dataPath[] = "dataRecorder"; //relative to sourcemod root

// -- Player related parameters ---
char g_time[64];
float startTime = 0.0;
int startTick;
int round;


public void OnPluginStart()
{
	switch(GetEngineVersion()) {
		case !Engine_CSGO:
			SetFailState("This plugin is only for CS:GO");
	}
	
	PrintToServer("DataRecorder - Plugin started");
	
	
	HookEvent("round_start", OnRoundStart, EventHookMode_PostNoCopy);
	HookEvent("round_end", OnRoundEnd, EventHookMode_Post);
	
	RegConsoleCmd("datarec_lsl", lslEnable);
	RegConsoleCmd("datarec_pipe", pipeEnable); // Only for UNIX systems
	RegConsoleCmd("datarec_csv", CSVEnable);
	RegConsoleCmd("datarec_auto", autoRecEnable);	
	RegConsoleCmd("datarec_start", StartRecording);
	RegConsoleCmd("datarec_server", ServerConsoleCommand);}


public void OnRoundStart(Handle event, const char[] name, bool dontBroadcast){
	round+=1;
	char roundStart[32];
	Format(roundStart, sizeof(roundStart),"Round %d start", round);

	char time[64];
	FormatTime(time, sizeof(time), "%H%M%S_%d%m%y", GetTime());
	g_time=time;

	if(autoRecordVal && !recording){
		ServerCommand("datarec_start");
	}
	
	if(lslEnableVal && recording){
		
		for(int i=0; i<sizeof(players);i++){
			push_stringSample(lslStreamIDs[i][2], roundStart);
			//PrintToServer("DataRecorder - Pushed marker player %d: %s", i, roundStart);			
		}
		lslSendMarkerInfo();
	}

	if((csvEnableVal||pipeEnableVal) && recording){
		writeCSVextraInfo();
	}
	
}

public Action OnRoundEnd(Handle event, const char[] name, bool dontBroadcast){
	if(lslEnableVal && recording){
		char roundEnd[32];
		Format(roundEnd, sizeof(roundEnd),"Round %d end", round);
		for(int i=0; i<sizeof(players);i++){
			push_stringSample(lslStreamIDs[i][2], roundEnd);
			//PrintToServer("DataRecorder - Pushed marker player %d: %s", i, roundEnd);
		}
	}
}

public bool OnClientConnect(int client, char[] rejectmsg, int maxlen){
	//int currConnect = 0;
	if (!IsClientSourceTV(client)){
		for(int i=0; i<sizeof(players); i++){
			if(players[i]==-1){
				players[i] = client;
				//currConnect = i;
				break;
			}else if (!IsFakeClient(client) && IsFakeClient(players[i])){
				//KickClient(players[i],"Go away bot...")
				players[i] = client;
				//currConnect = i;
				break;
			}
			else {
				if(i==sizeof(players)-1){
					return false;
				}
			}
		}
	}
	return true;
}

public void OnClientAuthorized(int client, const char[] auth){
	bool clientInList = false;
	for(int i=0; i<sizeof(players); i++){
		if (client==players[i]){
			clientInList = true;
			GetClientName(players[i], playerName[i], sizeof(playerName[]));
			CheckPlayerName(i);
			PrintToServer("DataRecorder - Player connected with name: %s is given index %d", playerName[i], i);
			break;
		}
	}
	if(!clientInList){
		PrintToServer("DataRecorder - New authorized client is NOT in the player list!")
	}
}

public void OnClientDisconnect(int client){
	for(int i=0; i<sizeof(players); i++){
		if(client==players[i]){
			PrintToServer("DataRecorder - Player disconnected with index %d", i);
			players[i] = -1;
			
			break;
		}
	}
}


public void mergeStringArray(char[][] stringArray, int arraySize, char[] delimiter, char[] mergedString, int stringSize){
	for(int i=0;i<arraySize; i++){
		Format(mergedString, stringSize, "%s%s",mergedString,stringArray[i]);
		if(i<arraySize-1){
			Format(mergedString, stringSize, "%s;",mergedString);
		}
	}

}

public void lslSendMarkerInfo(){
	for(int i=0; i<sizeof(players);i++){

		//Some useful marker data (dictionaries and other)
		char playerInfo[256];
		Format(playerInfo, sizeof(playerInfo),
			"Players: {idx:%d, name:%s, team:%d, isBot:%b}", 
			i, playerName[i], GetClientTeam(players[i]), IsFakeClient(players[i]));
		push_stringSample(lslStreamIDs[i][2], playerInfo);
		//PrintToServer("DataRecorder - Pushed marker player %d: %s", i, playerInfo)
	}
}

public void writeCSVextraInfo(){

	//Player Info
	char playerInfPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, playerInfPath, sizeof(playerInfPath), "%s/Players_%s.csv", dataPath, g_time);
	PrintToServer("DataRecorder - Creating csv file at %s ...", playerInfPath);
	File playerInfFile = OpenFile(playerInfPath,"a");
	if(!playerInfFile){
		PrintToServer("DataRecorder - Error opening csv file %s", playerInfPath);
	}else{
		playerInfFile.WriteLine("id;name;steamID;isBot");
		char playerInfRow[128];
		for(int i=0; i<sizeof(players);i++){
			Format(playerInfRow, sizeof(playerInfRow),"%d;%s;%d;%b", i, playerName[i], GetClientTeam(players[i]), IsFakeClient(players[i]));
			playerInfFile.WriteLine(playerInfRow);
		}
		playerInfFile.Close();
	}
}

public void lslInit(int i){
	int rate = RoundFloat(1/GetTickInterval());

	//Int data outlet
	char outletNameint[64];
	Format(outletNameint, sizeof(outletNameint), "IntData_%d",i);
	char streamIDint[64];

	char intChannelsString[(sizeof(lslIntChnLabels)-1)+sizeof(lslIntChnLabels)*sizeof(lslIntChnLabels[])];
	mergeStringArray(lslIntChnLabels, sizeof(lslIntChnLabels),";",intChannelsString, sizeof(intChannelsString));

	int chnNum = createLSLoutlet(outletNameint,"integer data", intChannelsString, rate, streamIDint);
	if(chnNum>0){
		lslStreamIDs[i][0] = streamIDint;
		PrintToServer("DataRecorder - Created LSL integer data outlet with name %s and %d number of channels",outletNameint,chnNum);
	}else{
		PrintToServer("DataRecorder - Problem creating LSL integer data outlet with name %s: %d",outletNameint, chnNum);
	}

	//Float data outlet
	char outletNamefloat[64];
	Format(outletNamefloat, sizeof(outletNamefloat), "FloatData_%d",i);
	char streamIDflt[64];

	char floatChannelsString[(sizeof(lslFltChnLabels)-1)+sizeof(lslFltChnLabels)*sizeof(lslFltChnLabels[])];
	mergeStringArray(lslFltChnLabels, sizeof(lslFltChnLabels),";",floatChannelsString, sizeof(floatChannelsString));

	chnNum = createLSLoutlet(outletNamefloat,"float data", floatChannelsString, rate, streamIDflt);
	if(chnNum>0){
		lslStreamIDs[i][1] = streamIDflt;
		PrintToServer("DataRecorder - Created LSL float data outlet with name %s and %d number of channels",outletNamefloat,chnNum);
	}else{
		PrintToServer("DataRecorder - Problem creating LSL float data outlet with name %s: %d",outletNamefloat,chnNum);
	}
	

	//String data outlet
	char outletNamestring[64];
	Format(outletNamestring, sizeof(outletNamestring), "MarkerData_%d",i);
	char streamIDmrk[64];
	chnNum = createLSLoutlet(outletNamestring,"string data", "", rate, streamIDmrk);
	if(chnNum==1){
		lslStreamIDs[i][2] = streamIDmrk;
		PrintToServer("DataRecorder - Created LSL marker data outlet with name %s",outletNamestring);
	}else{
		PrintToServer("DataRecorder - Problem creating LSL marker data outlet with name %s: %d",outletNamestring, chnNum);
	}	
	
}

public void lslInitAll(){
	for(int i=0; i<sizeof(players); i++){
		lslInit(i);
	}
	lslSendMarkerInfo();
}

public void lslClose(char[] streamID){
	//No implementation for this in the extension yet.
}

public void lslCloseAll(){
	for(int i=0; i<sizeof(lslStreamIDs); i++){
		for(int j=0;j<sizeof(lslStreamIDs[]);j++){
			lslClose(lslStreamIDs[i][j]);
		}
	}
}

public void pipeInit(int i){
	char pipename[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, pipename, sizeof(pipename), "%s/GameData_%d", dataPath, i);
	pipepath[i] = pipename;
	PrintToServer("DataRecorder - Creating csv file at %s ...", pipepath[i]);
	pipeHandles[i] = OpenFile(pipepath[i],"a");
	if(!pipeHandles[i]){
		PrintToServer("DataRecorder - Error opening pipe file %s", pipepath[i]);
		return;
	}
	
}

public void pipeInitAll(){
	for(int i=0; i<sizeof(players); i++){
		pipeClose(i);
		pipeInit(i);
	}
	writeCSVextraInfo();
}

public void pipeClose(int i){
	if(pipeHandles[i]!=null){
		pipeHandles[i].Close();
		pipeHandles[i] = null;
	}
}

public void pipeCloseAll(){
	for(int i = 0; i<sizeof(players); i++){
		pipeClose(i);
	}
}

public void csvInit(int i){
	char filename[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, filename, sizeof(filename), "%s/GameData_%d_%s.csv", dataPath, i, g_time);
	csvpath[i] = filename;
	PrintToServer("DataRecorder - Creating csv file at %s ...", csvpath[i]);
	csvHandles[i] = OpenFile(csvpath[i],"a");
	if(!csvHandles[i]){
		PrintToServer("DataRecorder - Error opening csv file %s", csvpath[i]);
		return;
	}
	
	char intChannelsString[(sizeof(lslIntChnLabels)-1)+sizeof(lslIntChnLabels)*sizeof(lslIntChnLabels[])];
	mergeStringArray(lslIntChnLabels, sizeof(lslIntChnLabels),";",intChannelsString, sizeof(intChannelsString));

	char floatChannelsString[(sizeof(lslFltChnLabels)-1)+sizeof(lslFltChnLabels)*sizeof(lslFltChnLabels[])];
	mergeStringArray(lslFltChnLabels, sizeof(lslFltChnLabels),";",floatChannelsString, sizeof(floatChannelsString));

	char csvLabels[sizeof(intChannelsString)+sizeof(floatChannelsString)+1];
	Format(csvLabels, sizeof(csvLabels),"%s;%s",intChannelsString,floatChannelsString)
	csvHandles[i].WriteLine(csvLabels);
}

public void csvInitAll(){
	char time[64];
	FormatTime(time, sizeof(time), "%H%M%S_%d%m%y", GetTime());
	g_time=time;
	for(int i=0; i<sizeof(players); i++){
		csvClose(i);
		csvInit(i);
	}
	writeCSVextraInfo();
}

public void csvClose(int i){
	if(csvHandles[i]!=null){
		csvHandles[i].Close();
		csvHandles[i] = null;
	}
}

public void csvCloseAll(){
	for(int i = 0; i<sizeof(players); i++){
		csvClose(i);
	}
}

public Action lslEnable(int client,int args){
	if(!lslEnableVal){
		lslEnableVal = true;
		if(recording){
			lslInitAll();
		}
		PrintToServer("DataRecorder - LSL recording enabled")
	}else{
		lslEnableVal = false;
		lslCloseAll();
		PrintToServer("DataRecorder - LSL recording disabled")
	}
	
	return Plugin_Handled;
}

public Action pipeEnable(int client, int args){
	if(!pipeEnableVal){
		//TODO maybe should check if this is a unix system, windows does not support pipes
		pipeEnableVal = true;
		if(recording){
			pipeInitAll();
		}
		PrintToServer("DataRecorder - Pipe recording enabled")
	}else{
		pipeEnableVal = false;
		pipeCloseAll()
		PrintToServer("DataRecorder - Pipe recording disabled")
	}
	
	return Plugin_Handled;
}

public Action CSVEnable(int client, int args){
	if(!csvEnableVal){
		csvEnableVal = true;
		if(recording){
			csvInitAll();
		}
		PrintToServer("DataRecorder - CSV recording enabled")
	}else{
		csvEnableVal = false;
		csvCloseAll();
		PrintToServer("DataRecorder - CSV recording disabled")
	}
	
	return Plugin_Handled;
}

public Action autoRecEnable(int client, int args){

	if(autoRecordVal){
		autoRecordVal = false;
		PrintToServer("DataRecorder - autorecord disabled");
	}else{
		autoRecordVal = true;
		PrintToServer("DataRecorder - autorecord enabled");
	}
	

	return Plugin_Handled;
}

public Action ServerConsoleCommand(int client, int args){
	char arg1[64];
	GetCmdArg(1, arg1, sizeof(arg1));
	
	PrintToServer("DataRecorder - Server console command: %s", arg1);
	
	ServerCommand(arg1);
	
	return Plugin_Handled;
}

public void CheckPlayerName(int clientIndex){
	char oldPlayerName[128];
	oldPlayerName = playerName[clientIndex];
	int changes = 0;
	changes += ReplaceString(playerName[clientIndex], sizeof(playerName[]), "_", "-", false);
	changes += ReplaceString(playerName[clientIndex], sizeof(playerName[]), "/", "-", false);
	changes += ReplaceString(playerName[clientIndex], sizeof(playerName[]), "\\", "-", false);
	changes += ReplaceString(playerName[clientIndex], sizeof(playerName[]), ".", "-", false);
	if(changes>0){
		PrintToServer("DataRecorder - The initial player name (%s) was replaced by %s because some characters [_,\\,/,.] were not allowed", oldPlayerName, playerName[clientIndex]);
	}
}


public Action StartRecording(int client, int args){
	
	if (csvEnableVal){
		csvInitAll();
	}
	if (pipeEnableVal){
		pipeInitAll();
	}
	if (lslEnableVal){
		lslInitAll();
	}

	PrintToServer("DataRecorder -  started recording");
	
	recording = true;
	return Plugin_Handled;
}

public Action StopRecording(Handle timer){
	//UnhookEvent("player_death", OnPlayerDeathTOT);
	
	recording=false;
	started=false;
	
	csvCloseAll();
	pipeCloseAll();
	lslCloseAll();
	
	PrintToServer("DataRecorder -  stopped recording");
	return Plugin_Handled;
}

public Action Exit(int args){
	recording=false;
	started=false;
	
	csvCloseAll();
	pipeCloseAll();
	lslCloseAll();
	
	PrintToServer("DataRecorder -  stop recording for exit reason");
	return Plugin_Handled;
}



// Send the data on each game frame
public void OnGameFrame(){
	if(recording && !started){
		started=true;
	} else if (recording){

		float time = GetEngineTime();

		for(int i = 0; i<sizeof(players); i++){
			if(players[i]!=-1){

				// time
				lslFltSample[i][view_as<LSLfltChnIndex>(Time)]=time;

				// x y z Position
				float position[3];
				GetClientEyePosition(players[i], position);
				lslFltSample[i][view_as<LSLfltChnIndex>(PositionX)] = position[0];
				lslFltSample[i][view_as<LSLfltChnIndex>(PositionY)] = position[1];
				lslFltSample[i][view_as<LSLfltChnIndex>(PositionZ)] = position[2];

				
				// Health
				lslIntSample[i][view_as<LSLintChnIndex>(Health)] = GetClientHealth(players[i]);
				
				// Armor
				lslIntSample[i][view_as<LSLintChnIndex>(Armor)] = GetClientArmor(players[i]);

				// Score
				lslIntSample[i][view_as<LSLintChnIndex>(Score)] = CS_GetClientContributionScore(players[i]);
				
				
				if(csvEnableVal || pipeEnableVal){
				
					char row[10240];
					for(int j=0; j<view_as<int>(LSLintChnIndex); j++){
						Format(row, sizeof(row), "%s%d;", row, lslIntSample[i][j]);
					}
					for(int j=0; j<view_as<int>(LSLfltChnIndex); j++){
						Format(row, sizeof(row), "%s%f", row, lslFltSample[i][j]);
						if(j<view_as<int>(LSLfltChnIndex)-1){
							Format(row, sizeof(row), "%s;", row);
						}
					}

					if(csvEnableVal && csvHandles[i]){
						csvHandles[i].WriteLine(row);
					}
					if(pipeEnableVal && pipeHandles[i]){
						pipeHandles[i].WriteLine(row);
					}
					
				}
					
				if(lslEnableVal){
					push_intSample(lslStreamIDs[i][0], lslIntSample[i]);
					push_floatSample(lslStreamIDs[i][1], lslFltSample[i]);
				}
			
			}
		}

	}
}

